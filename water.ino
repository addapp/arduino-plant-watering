
#include <stdio.h>

// moisture sensor variables
int volt = 0;
int dry = 2.0 * 1024 / 5;
int moist = 1.4 * 1024 / 5;

// time variables
unsigned long time = 0;
unsigned int hour = 0;

// delays
unsigned long pumpDelay = 3000;
unsigned long reCheckInterval = 5000;//600000;
unsigned long afterPumpDelay = 3600000;

// pump vars
float ML_PER_SEC = 30;


// relays
typedef struct {
  unsigned int pump_pin;
  unsigned int sensor_pin;
  bool already_pumped;
  float pump_volume;
  float pump_time;
} pump_t;

unsigned int pumpA = 0;
unsigned int pumpB = 1;
unsigned int pumpC = 2;
pump_t pumpState[3] = {{
  .pump_pin = 2,
  .sensor_pin = A0, 
  .pump_volume = 30
}, {
  .pump_pin = 4,
  .sensor_pin = A2, 
  .pump_volume = 30
}, {
  .pump_pin = 7,
  .sensor_pin = A5, 
  .pump_volume = 60
}};
unsigned int PUMP_TRIES_MAX = 10; // how often the pump should be turned on if it's dry

void calcPumpTime(unsigned int pump) {
  pumpState[pump].pump_time = pumpState[pump].pump_volume / ML_PER_SEC;
  Serial.print("Time: ");
  Serial.println(pumpState[pump].pump_time);
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(LED_BUILTIN, OUTPUT);

  // register output pins to transistors
  pinMode(pumpState[pumpA].pump_pin, OUTPUT);
  pinMode(pumpState[pumpB].pump_pin, OUTPUT);
  pinMode(pumpState[pumpC].pump_pin, OUTPUT); 

  // calculate pump times
  for (unsigned int pump = pumpA; pump <= pumpC; pump++) {
    calcPumpTime(pump);
  }
}

//void blink(unsigned long duration, int times) {
//  for (int i = 0; i < times; i++) {
//    digitalWrite(LED_BUILTIN, HIGH);
//    delay(duration);
//    digitalWrite(LED_BUILTIN, LOW);
//    delay(duration);
//  }
//}
//
//void signalDry(unsigned long pump) {
//  Serial.print("Plant (");
//  Serial.print(pump);
//  Serial.println(") needs to be watered");
//
//  blink(100, 10);
//}
//
//void signalMoist(unsigned long pump) {
//  Serial.print("Plant (");
//  Serial.print(pump);
//  Serial.println(") does not need to be watered");
//
//  
//  blink(1000, 5);
//}
//
//void signalWait(unsigned long pump) {
//  Serial.print("Pump (");
//  Serial.print(pump);
//  Serial.println(") is waiting for another check");
//
//  
//  blink(100, 5);
//}
//
//void signalDone(unsigned long pump) {
//  Serial.print("Pump (");
//  Serial.print(pump);
//  Serial.println(") is done for today");
//
//  
//  blink(500, 2);
//}
//
//void startPump(unsigned int pump) {
//  digitalWrite(pumpState[pump][PUMP_OUTPUT], HIGH);
//  delay(pumpState[pump][PUMP_TIME]);
//  digitalWrite(pumpState[pump][PUMP_OUTPUT], LOW);
//}
//
//void signalVoltage(unsigned int pump, int volt) {
//  Serial.print("Sensor (");
//  Serial.print(pump);
//  Serial.print(") reads (");
//  Serial.print(volt);
//  Serial.println(") volt");
//}
//
//bool checkMoisture(unsigned int pump) {
//  volt = analogRead(pumpState[pump][SENSOR_INPUT]);
//  signalVoltage(pump, volt);
//  
//  if (volt > dry) {
//    // water the plant and check again in 10 minutes
//    signalDry(pump);
//    startPump(pump);
//    return true;
//  } else {
////    digitalWrite(LED_BUILTIN, LOW);
//    // don't water the plant and check again tomorrow
//    signalMoist(pump);
//    return false;
//  }
//}

void loop() {
//  time = millis();
//  hour = time / 3600000;  
//  
////  if (hour % 24 == 0) {
//    // check the moisture level
//    Serial.println("Starting pumps");
//
//    // for now only test with one pump
//    for (unsigned int pump = pumpA; pump <= pumpA; pump++) {
//      for (unsigned int pumpTries = 0; pumpTries < PUMP_TRIES_MAX; pumpTries++) {
//        if (checkMoisture(pump)) {
//          signalWait(pump);
//          delay(reCheckInterval);
//        } else {
//          break;
//        }
//      }
//      signalDone(pump);
//    }
////  }
//
//  delay(afterPumpDelay);
}