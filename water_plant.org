* On RPI
raspivid -o - -t 0 -w 640 -h 480 -fps 10 -b 4000000 -g 50 | ffmpeg -f h264 -i - -vcodec copy -g 50 -strict experimental -f flv rtmp://192.168.188.26:1935/live/12345

full command line is
raspivid -o - -t 0 -w 1920 -h 1080 -fps 25 -b 4000000 -g 50 | /home/pi/ffmpeg -re -ar 44100 -ac 2 -acodec pcm_s16le -f s16le -ac 2 -i /dev/zero -f h264 -i - -vcodec copy -acodec aac -ab 128k -g 50 -strict experimental -f flv rtmp://your_RTMP_server_address/your_stream_keyk

but I have to find out what all the arguments are doing

* On RTMP server
https://hub.docker.com/r/tiangolo/nginx-rtmp/
docker run --rm -it -p 1935:1935 --name nginx-rtmp tiangolo/nginx-rtmp

* On RTMP client
open rtmp://192.168.188.26:1935/live/12345 in vlc
